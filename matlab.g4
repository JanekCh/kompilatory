grammar matlab;

program : (functions)? basic;

functions : (new_function)+;

new_function : FUNCTION (identyficator|(OPEN_SQ_BRACKET identyficator (point identyficator)* CLOSE_SQ_BRACKET))  assign identyficator OPEN_BRACKET functrion_args CLOSE_BRACKET basic END;

functrion_args : (identyficator (point identyficator)*)?;

basic : (control_function|basic_expr|assign_expr) (basic)?;

control_function : (if_function|for_function|while_function);

loop_stmt
    :break_stmt (loop_stmt)?
    |continue_stmt (loop_stmt)?
    |basic (loop_stmt)?;

while_function : WHILE while_expr loop_stmt END;
while_expr : cond_expr;

for_function : FOR for_stmt loop_stmt END;

for_stmt : identyficator assign for_expr;
for_expr : (numb|identyficator|function) colon (numb|identyficator) (colon (numb|identyficator))?;

if_function : if_stmt (elseif_stmt)* (else_stmt)? END;

if_stmt : IF while_expr basic;
elseif_stmt : ELSEIF while_expr basic;
else_stmt : ELSE basic;

argument : expr (point argument)?; // PROBLEM!!!!

arguments : (argument)?;

function : identyficator OPEN_BRACKET arguments CLOSE_BRACKET;

assign_expr : (identyficator|array) assign (expr|function) semicolon;

expr
    :expr add_expr expr
    |expr mul_expr expr
    |expr pow_expr expr
    |OPEN_BRACKET expr CLOSE_BRACKET
    |basic_expr;

basic_expr : (array|identyficator|numb);

array : OPEN_SQ_BRACKET (numb|identyficator) (((point|colon) numb) | ((point) identyficator))* CLOSE_SQ_BRACKET;

cond_expr
    :cond_expr and_expr cond_expr
    |cond_expr or_expr cond_expr
    |expr comparison_expr expr
    |OPEN_BRACKET cond_expr CLOSE_BRACKET;

add_expr
    :ADD
    |SUB;

mul_expr
    :MUL
    |DIV
    |ARRAY_MUL
    |ARRAY_DIV;

pow_expr
    :POW
    |ARRAY_POW;

comparison_expr
    :LOWER
    |LOWER_EQUAL
    |GREATER
    |GREATER_EQUAL
    |EQUAL
    |NOT_EQUAL;

break_stmt : BREAK semicolon;
continue_stmt : CONTINUE semicolon;

numb
    :floating
    |integer;

floating : NUMBER* '.' NUMBER+;
integer : NUMBER+;
identyficator : LETTER (LETTER|NUMBER|'_'|'.')*;
colon : ':';
semicolon : ';';
assign : '=';
and_expr : '&&';
or_expr : '||';
point : ',';



ADD : '+';
SUB : '-';
MUL : '*';
DIV : '/';
POW : '^';
NOT : '~';
ARRAY_MUL : '.*';
ARRAY_DIV : './';
ARRAY_POW : '.^';

FUNCTION : 'function';
FOR : 'for';
WHILE : 'while';
IF : 'if';
ELSE : 'else';
ELSEIF : 'elseif';
END : 'end';
RETURN : 'return';
BREAK : 'break';
CONTINUE: 'continue';

LOWER : '<';
GREATER : '>';
LOWER_EQUAL : '<=';
GREATER_EQUAL : '>=';
EQUAL : '==';
NOT_EQUAL : '~=';

OPEN_BRACKET : '(';
CLOSE_BRACKET : ')';
OPEN_SQ_BRACKET : '[';
CLOSE_SQ_BRACKET : ']';

NUMBER : [0-9];
LETTER : 'a'..'z' | 'A'..'Z';
WS:[ \t\r\n]+ -> skip ;
