# Generated from C:/Users/Janek/Kompilatory/Matlab2Python\matlab.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .matlabParser import matlabParser
else:
    from matlabParser import matlabParser

# This class defines a complete listener for a parse tree produced by matlabParser.
class matlabListener(ParseTreeListener):

    # Enter a parse tree produced by matlabParser#program.
    def enterProgram(self, ctx:matlabParser.ProgramContext):
        pass

    # Exit a parse tree produced by matlabParser#program.
    def exitProgram(self, ctx:matlabParser.ProgramContext):
        pass


    # Enter a parse tree produced by matlabParser#functions.
    def enterFunctions(self, ctx:matlabParser.FunctionsContext):
        pass

    # Exit a parse tree produced by matlabParser#functions.
    def exitFunctions(self, ctx:matlabParser.FunctionsContext):
        pass


    # Enter a parse tree produced by matlabParser#new_function.
    def enterNew_function(self, ctx:matlabParser.New_functionContext):
        pass

    # Exit a parse tree produced by matlabParser#new_function.
    def exitNew_function(self, ctx:matlabParser.New_functionContext):
        pass


    # Enter a parse tree produced by matlabParser#functrion_args.
    def enterFunctrion_args(self, ctx:matlabParser.Functrion_argsContext):
        pass

    # Exit a parse tree produced by matlabParser#functrion_args.
    def exitFunctrion_args(self, ctx:matlabParser.Functrion_argsContext):
        pass


    # Enter a parse tree produced by matlabParser#basic.
    def enterBasic(self, ctx:matlabParser.BasicContext):
        pass

    # Exit a parse tree produced by matlabParser#basic.
    def exitBasic(self, ctx:matlabParser.BasicContext):
        pass


    # Enter a parse tree produced by matlabParser#control_function.
    def enterControl_function(self, ctx:matlabParser.Control_functionContext):
        pass

    # Exit a parse tree produced by matlabParser#control_function.
    def exitControl_function(self, ctx:matlabParser.Control_functionContext):
        pass


    # Enter a parse tree produced by matlabParser#loop_stmt.
    def enterLoop_stmt(self, ctx:matlabParser.Loop_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#loop_stmt.
    def exitLoop_stmt(self, ctx:matlabParser.Loop_stmtContext):
        pass


    # Enter a parse tree produced by matlabParser#while_function.
    def enterWhile_function(self, ctx:matlabParser.While_functionContext):
        pass

    # Exit a parse tree produced by matlabParser#while_function.
    def exitWhile_function(self, ctx:matlabParser.While_functionContext):
        pass


    # Enter a parse tree produced by matlabParser#while_expr.
    def enterWhile_expr(self, ctx:matlabParser.While_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#while_expr.
    def exitWhile_expr(self, ctx:matlabParser.While_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#for_function.
    def enterFor_function(self, ctx:matlabParser.For_functionContext):
        pass

    # Exit a parse tree produced by matlabParser#for_function.
    def exitFor_function(self, ctx:matlabParser.For_functionContext):
        pass


    # Enter a parse tree produced by matlabParser#for_stmt.
    def enterFor_stmt(self, ctx:matlabParser.For_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#for_stmt.
    def exitFor_stmt(self, ctx:matlabParser.For_stmtContext):
        pass


    # Enter a parse tree produced by matlabParser#for_expr.
    def enterFor_expr(self, ctx:matlabParser.For_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#for_expr.
    def exitFor_expr(self, ctx:matlabParser.For_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#if_function.
    def enterIf_function(self, ctx:matlabParser.If_functionContext):
        pass

    # Exit a parse tree produced by matlabParser#if_function.
    def exitIf_function(self, ctx:matlabParser.If_functionContext):
        pass


    # Enter a parse tree produced by matlabParser#if_stmt.
    def enterIf_stmt(self, ctx:matlabParser.If_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#if_stmt.
    def exitIf_stmt(self, ctx:matlabParser.If_stmtContext):
        pass


    # Enter a parse tree produced by matlabParser#elseif_stmt.
    def enterElseif_stmt(self, ctx:matlabParser.Elseif_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#elseif_stmt.
    def exitElseif_stmt(self, ctx:matlabParser.Elseif_stmtContext):
        pass


    # Enter a parse tree produced by matlabParser#else_stmt.
    def enterElse_stmt(self, ctx:matlabParser.Else_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#else_stmt.
    def exitElse_stmt(self, ctx:matlabParser.Else_stmtContext):
        pass


    # Enter a parse tree produced by matlabParser#argument.
    def enterArgument(self, ctx:matlabParser.ArgumentContext):
        pass

    # Exit a parse tree produced by matlabParser#argument.
    def exitArgument(self, ctx:matlabParser.ArgumentContext):
        pass


    # Enter a parse tree produced by matlabParser#arguments.
    def enterArguments(self, ctx:matlabParser.ArgumentsContext):
        pass

    # Exit a parse tree produced by matlabParser#arguments.
    def exitArguments(self, ctx:matlabParser.ArgumentsContext):
        pass


    # Enter a parse tree produced by matlabParser#function.
    def enterFunction(self, ctx:matlabParser.FunctionContext):
        pass

    # Exit a parse tree produced by matlabParser#function.
    def exitFunction(self, ctx:matlabParser.FunctionContext):
        pass


    # Enter a parse tree produced by matlabParser#assign_expr.
    def enterAssign_expr(self, ctx:matlabParser.Assign_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#assign_expr.
    def exitAssign_expr(self, ctx:matlabParser.Assign_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#expr.
    def enterExpr(self, ctx:matlabParser.ExprContext):
        pass

    # Exit a parse tree produced by matlabParser#expr.
    def exitExpr(self, ctx:matlabParser.ExprContext):
        pass


    # Enter a parse tree produced by matlabParser#basic_expr.
    def enterBasic_expr(self, ctx:matlabParser.Basic_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#basic_expr.
    def exitBasic_expr(self, ctx:matlabParser.Basic_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#array.
    def enterArray(self, ctx:matlabParser.ArrayContext):
        pass

    # Exit a parse tree produced by matlabParser#array.
    def exitArray(self, ctx:matlabParser.ArrayContext):
        pass


    # Enter a parse tree produced by matlabParser#cond_expr.
    def enterCond_expr(self, ctx:matlabParser.Cond_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#cond_expr.
    def exitCond_expr(self, ctx:matlabParser.Cond_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#add_expr.
    def enterAdd_expr(self, ctx:matlabParser.Add_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#add_expr.
    def exitAdd_expr(self, ctx:matlabParser.Add_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#mul_expr.
    def enterMul_expr(self, ctx:matlabParser.Mul_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#mul_expr.
    def exitMul_expr(self, ctx:matlabParser.Mul_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#pow_expr.
    def enterPow_expr(self, ctx:matlabParser.Pow_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#pow_expr.
    def exitPow_expr(self, ctx:matlabParser.Pow_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#comparison_expr.
    def enterComparison_expr(self, ctx:matlabParser.Comparison_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#comparison_expr.
    def exitComparison_expr(self, ctx:matlabParser.Comparison_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#break_stmt.
    def enterBreak_stmt(self, ctx:matlabParser.Break_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#break_stmt.
    def exitBreak_stmt(self, ctx:matlabParser.Break_stmtContext):
        pass


    # Enter a parse tree produced by matlabParser#continue_stmt.
    def enterContinue_stmt(self, ctx:matlabParser.Continue_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#continue_stmt.
    def exitContinue_stmt(self, ctx:matlabParser.Continue_stmtContext):
        pass


    # Enter a parse tree produced by matlabParser#numb.
    def enterNumb(self, ctx:matlabParser.NumbContext):
        pass

    # Exit a parse tree produced by matlabParser#numb.
    def exitNumb(self, ctx:matlabParser.NumbContext):
        pass


    # Enter a parse tree produced by matlabParser#floating.
    def enterFloating(self, ctx:matlabParser.FloatingContext):
        pass

    # Exit a parse tree produced by matlabParser#floating.
    def exitFloating(self, ctx:matlabParser.FloatingContext):
        pass


    # Enter a parse tree produced by matlabParser#integer.
    def enterInteger(self, ctx:matlabParser.IntegerContext):
        pass

    # Exit a parse tree produced by matlabParser#integer.
    def exitInteger(self, ctx:matlabParser.IntegerContext):
        pass


    # Enter a parse tree produced by matlabParser#identyficator.
    def enterIdentyficator(self, ctx:matlabParser.IdentyficatorContext):
        pass

    # Exit a parse tree produced by matlabParser#identyficator.
    def exitIdentyficator(self, ctx:matlabParser.IdentyficatorContext):
        pass


    # Enter a parse tree produced by matlabParser#colon.
    def enterColon(self, ctx:matlabParser.ColonContext):
        pass

    # Exit a parse tree produced by matlabParser#colon.
    def exitColon(self, ctx:matlabParser.ColonContext):
        pass


    # Enter a parse tree produced by matlabParser#semicolon.
    def enterSemicolon(self, ctx:matlabParser.SemicolonContext):
        pass

    # Exit a parse tree produced by matlabParser#semicolon.
    def exitSemicolon(self, ctx:matlabParser.SemicolonContext):
        pass


    # Enter a parse tree produced by matlabParser#assign.
    def enterAssign(self, ctx:matlabParser.AssignContext):
        pass

    # Exit a parse tree produced by matlabParser#assign.
    def exitAssign(self, ctx:matlabParser.AssignContext):
        pass


    # Enter a parse tree produced by matlabParser#and_expr.
    def enterAnd_expr(self, ctx:matlabParser.And_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#and_expr.
    def exitAnd_expr(self, ctx:matlabParser.And_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#or_expr.
    def enterOr_expr(self, ctx:matlabParser.Or_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#or_expr.
    def exitOr_expr(self, ctx:matlabParser.Or_exprContext):
        pass


    # Enter a parse tree produced by matlabParser#point.
    def enterPoint(self, ctx:matlabParser.PointContext):
        pass

    # Exit a parse tree produced by matlabParser#point.
    def exitPoint(self, ctx:matlabParser.PointContext):
        pass


