import gen.matlabParser
from gen.matlabListener import matlabListener
from gen.matlabParser import matlabParser


class matlabListenerImplementation(matlabListener):
    my_list = []

    # Enter a parse tree produced by matlabParser#program.
    def enterProgram(self, ctx: matlabParser.ProgramContext):
        pass

    # Exit a parse tree produced by matlabParser#program.
    def exitProgram(self, ctx: matlabParser.ProgramContext):
        for ind in range(1, len(self.my_list) - 1):
            if self.my_list[ind] in ["elif ", "else "]:
                self.my_list.insert(ind - 1, "")
                del self.my_list[ind]
        index = 0
        to_save = []
        while index < len(self.my_list):
            to_save.append(self.my_list[index])
            index += 1
        for symbol in to_save:
            print(symbol, end="")
        with open('out.txt', 'w') as f:
            f.write("%s" % "import numpy as np \n")
            for item in self.my_list:
                f.write("%s" % item)

    # Enter a parse tree produced by matlabParser#functions.
    def enterFunctions(self, ctx: matlabParser.FunctionsContext):
        pass

    # Exit a parse tree produced by matlabParser#functions.
    def exitFunctions(self, ctx: matlabParser.FunctionsContext):
        pass

    # Enter a parse tree produced by matlabParser#new_function.
    def enterNew_function(self, ctx: matlabParser.New_functionContext):
        self.my_list.append("function start")
        self.my_list.append("def ")

    # Exit a parse tree produced by matlabParser#new_function.
    def exitNew_function(self, ctx: matlabParser.New_functionContext):
        return_index = index = len(self.my_list) - 1
        return_list = []

        while not self.my_list[return_index - 1] == "def ":
            return_index -= 1

        while not self.my_list[return_index] == " = ":
            return_list.append(self.my_list[return_index])
            del self.my_list[return_index]
        del self.my_list[return_index]
        if len(return_list) > 1:
            return_list.insert(0, "[")
            return_list.append("]")
        # for i in range(1, len(return_list) - 2):
        #     return_list.insert(i*2, ", ")

        self.my_list.append("return ")

        for symbol in return_list:
            self.my_list.append(symbol)
        self.my_list.append("\n")
        index = len(self.my_list) - 1
        first = 0
        while not self.my_list[index] == "function start":
            if self.my_list[index] == "\n":
                if first == 0:
                    first = 1
                else:
                    self.my_list.insert(index + 1, "\t")
            index -= 1
        del self.my_list[index]

    # Enter a parse tree produced by matlabParser#functrion_args.
    def enterFunctrion_args(self, ctx: matlabParser.Functrion_argsContext):
        self.my_list.append("( ")

    # Exit a parse tree produced by matlabParser#functrion_args.
    def exitFunctrion_args(self, ctx: matlabParser.Functrion_argsContext):
        self.my_list.append(") ")
        self.my_list.append(": ")
        self.my_list.append("\n")

    # Enter a parse tree produced by matlabParser#basic.
    def enterBasic(self, ctx: matlabParser.BasicContext):
        pass

    # Exit a parse tree produced by matlabParser#basic.
    def exitBasic(self, ctx: matlabParser.BasicContext):
        pass

    # Enter a parse tree produced by matlabParser#loop_stmt.
    def enterLoop_stmt(self, ctx: matlabParser.Loop_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#loop_stmt.
    def exitLoop_stmt(self, ctx: matlabParser.Loop_stmtContext):
        pass

    # Enter a parse tree produced by matlabParser#control_function.
    def enterControl_function(self, ctx: matlabParser.Control_functionContext):
        self.my_list.append("function start")

    # Exit a parse tree produced by matlabParser#control_function.
    def exitControl_function(self, ctx: matlabParser.Control_functionContext):
        index = len(self.my_list) - 1
        first = 0
        while not self.my_list[index] == "function start":
            if self.my_list[index] == "\n":
                if first == 0:
                    first = 1
                else:
                    self.my_list.insert(index + 1, "\t")
            index -= 1
        del self.my_list[index]

    # Enter a parse tree produced by matlabParser#while_function.
    def enterWhile_function(self, ctx: matlabParser.While_functionContext):
        self.my_list.append("while ")

    # Exit a parse tree produced by matlabParser#while_function.
    def exitWhile_function(self, ctx: matlabParser.While_functionContext):
        pass

    # Enter a parse tree produced by matlabParser#while_expr.
    def enterWhile_expr(self, ctx: matlabParser.While_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#while_expr.
    def exitWhile_expr(self, ctx: matlabParser.While_exprContext):
        self.my_list.append(": ")
        self.my_list.append("\n")

    # Enter a parse tree produced by matlabParser#for_function.
    def enterFor_function(self, ctx: matlabParser.For_functionContext):
        self.my_list.append("for ")

    # Exit a parse tree produced by matlabParser#for_function.
    def exitFor_function(self, ctx: matlabParser.For_functionContext):
        pass

    # Enter a parse tree produced by matlabParser#for_stmt.
    def enterFor_stmt(self, ctx: matlabParser.For_stmtContext):
        pass

    # Exit a parse tree produced by matlabParser#for_stmt.
    def exitFor_stmt(self, ctx: matlabParser.For_stmtContext):
        self.my_list.append(": ")
        self.my_list.append("\n")

    # Enter a parse tree produced by matlabParser#for_expr.
    def enterFor_expr(self, ctx: matlabParser.For_exprContext):
        del self.my_list[-1]
        self.my_list.append(" in range (")

    # Exit a parse tree produced by matlabParser#for_expr.
    def exitFor_expr(self, ctx: matlabParser.For_exprContext):
        index = len(self.my_list) - 1
        while not self.my_list[index] == "for ":
            if self.my_list[index] == ": ":
                del self.my_list[index]
                self.my_list.insert(index, ", ")
            index -= 1
        self.my_list.append(") ")

    # Enter a parse tree produced by matlabParser#if_function.
    def enterIf_function(self, ctx: matlabParser.If_functionContext):
        pass
        # self.my_list.append("if start")

    # Exit a parse tree produced by matlabParser#if_function.
    def exitIf_function(self, ctx: matlabParser.If_functionContext):
        pass

    # Enter a parse tree produced by matlabParser#argument.
    def enterArgument(self, ctx: matlabParser.ArgumentContext):
        pass

    # Exit a parse tree produced by matlabParser#argument.
    def exitArgument(self, ctx: matlabParser.ArgumentContext):
        pass

    # Enter a parse tree produced by matlabParser#arguments.
    def enterArguments(self, ctx: matlabParser.ArgumentsContext):
        self.my_list.append("( ")

    # Exit a parse tree produced by matlabParser#arguments.
    def exitArguments(self, ctx: matlabParser.ArgumentsContext):
        self.my_list.append(") ")

    # Enter a parse tree produced by matlabParser#function.
    def enterFunction(self, ctx: matlabParser.FunctionContext):
        pass

    # Exit a parse tree produced by matlabParser#function.
    def exitFunction(self, ctx: matlabParser.FunctionContext):
        pass

    # Enter a parse tree produced by matlabParser#expr.
    def enterExpr(self, ctx: matlabParser.ExprContext):
        pass

    # Exit a parse tree produced by matlabParser#expr.
    def exitExpr(self, ctx: matlabParser.ExprContext):
        pass

    # Enter a parse tree produced by matlabParser#basic_expr.
    def enterBasic_expr(self, ctx: matlabParser.Basic_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#basic_expr.
    def exitBasic_expr(self, ctx: matlabParser.Basic_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#array.
    def enterArray(self, ctx: matlabParser.ArrayContext):
        if self.my_list[-1] == " = ":
            self.my_list.append("np.array( ")
        self.my_list.append("[ ")

    # Exit a parse tree produced by matlabParser#array.
    def exitArray(self, ctx: matlabParser.ArrayContext):
        if self.my_list[len(self.my_list) - 1] == ", ":
            del self.my_list[-1]
        self.my_list.append("] ")
        index = len(self.my_list) - 1
        while index > 0:
            if self.my_list[index] == "np.array( ":
                self.my_list.append(") ")
            elif self.my_list[index] == "\n":
                break
            index -= 1

    # Enter a parse tree produced by matlabParser#if_stmt.
    def enterIf_stmt(self, ctx: matlabParser.If_stmtContext):
        self.my_list.append("if ")

    # Exit a parse tree produced by matlabParser#if_stmt.
    def exitIf_stmt(self, ctx: matlabParser.If_stmtContext):
        pass

    # Enter a parse tree produced by matlabParser#elseif_stmt.
    def enterElseif_stmt(self, ctx: matlabParser.Elseif_stmtContext):
        self.my_list.append("elif ")

    # Exit a parse tree produced by matlabParser#elseif_stmt.
    def exitElseif_stmt(self, ctx: matlabParser.Elseif_stmtContext):
        pass

    # Enter a parse tree produced by matlabParser#else_stmt.
    def enterElse_stmt(self, ctx: matlabParser.Else_stmtContext):
        self.my_list.append("else ")
        self.my_list.append(": ")
        self.my_list.append("\n")

    # Exit a parse tree produced by matlabParser#else_stmt.
    def exitElse_stmt(self, ctx: matlabParser.Else_stmtContext):
        pass

    # Enter a parse tree produced by matlabParser#assign_expr.
    def enterAssign_expr(self, ctx: matlabParser.Assign_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#assign.
    def enterAssign(self, ctx: matlabParser.AssignContext):
        self.my_list.append(" = ")

    # Exit a parse tree produced by matlabParser#assign.
    def exitAssign(self, ctx: matlabParser.AssignContext):
        pass

    # Exit a parse tree produced by matlabParser#assign_expr.
    def exitAssign_expr(self, ctx: matlabParser.Assign_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#cond_expr.
    def enterCond_expr(self, ctx: matlabParser.Cond_exprContext):
        pass

    # Exit a parse tree produced by matlabParser#cond_expr.
    def exitCond_expr(self, ctx: matlabParser.Cond_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#comparison_expr.
    def enterComparison_expr(self, ctx: matlabParser.Comparison_exprContext):
        comp = ctx.getText()
        if comp.__eq__("~="):
            self.my_list.append(" != ")
        else:
            self.my_list.append(" " + comp + " ")

    # Exit a parse tree produced by matlabParser#comparison_expr.
    def exitComparison_expr(self, ctx: matlabParser.Comparison_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#break_stmt.
    def enterBreak_stmt(self, ctx: matlabParser.Break_stmtContext):
        self.my_list.append("break ")

    # Exit a parse tree produced by matlabParser#break_stmt.
    def exitBreak_stmt(self, ctx: matlabParser.Break_stmtContext):
        pass

    # Enter a parse tree produced by matlabParser#continue_stmt.
    def enterContinue_stmt(self, ctx: matlabParser.Continue_stmtContext):
        self.my_list.append("continue ")

    # Exit a parse tree produced by matlabParser#continue_stmt.
    def exitContinue_stmt(self, ctx: matlabParser.Continue_stmtContext):
        pass

    # Enter a parse tree produced by matlabParser#numb.
    def enterNumb(self, ctx: matlabParser.NumbContext):
        pass

    # Exit a parse tree produced by matlabParser#numb.
    def exitNumb(self, ctx: matlabParser.NumbContext):
        pass

    # Enter a parse tree produced by matlabParser#floating.
    def enterFloating(self, ctx: matlabParser.FloatingContext):
        floa = ctx.getText()
        self.my_list.append(floa + " ")

    # Exit a parse tree produced by matlabParser#floating.
    def exitFloating(self, ctx: matlabParser.FloatingContext):
        pass

    # Enter a parse tree produced by matlabParser#integer.
    def enterInteger(self, ctx: matlabParser.IntegerContext):
        integ = ctx.getText()
        if self.my_list[len(self.my_list) - 1] == ": " and self.my_list.count("[ ") > self.my_list.count("] "):
            del self.my_list[-1]
            for i in range(int(self.my_list[len(self.my_list) - 2]), int(integ) + 1):
                self.my_list.append(str(i))
                self.my_list.append(", ")
        elif self.my_list.count("[ ") > self.my_list.count("] "):
            self.my_list.append(integ + " ")
        else:
            self.my_list.append(integ + " ")

    # Exit a parse tree produced by matlabParser#integer.
    def exitInteger(self, ctx: matlabParser.IntegerContext):
        pass

    # Enter a parse tree produced by matlabParser#id.
    def enterIdentyficator(self, ctx: matlabParser.IdentyficatorContext):
        id = ctx.getText()
        self.my_list.append(id)

    # Exit a parse tree produced by matlabParser#id.
    def exitIdentyficator(self, ctx: matlabParser.IdentyficatorContext):
        pass

        # Enter a parse tree produced by matlabParser#add_expr.

    def enterAdd_expr(self, ctx: matlabParser.Add_exprContext):
        sign = ctx.getText()
        self.my_list.append(" " + sign + " ")

        # Exit a parse tree produced by matlabParser#add_expr.

    def exitAdd_expr(self, ctx: matlabParser.Add_exprContext):
        pass

        # Enter a parse tree produced by matlabParser#mul_expr.

    def enterMul_expr(self, ctx: matlabParser.Mul_exprContext):
        sign = ctx.getText()
        if sign == "*" or sign == ".*":
            self.my_list.append(" * ")
        elif sign == "/" or sign == "./":
            self.my_list.append(" / ")

        # Exit a parse tree produced by matlabParser#mul_expr.

    def exitMul_expr(self, ctx: matlabParser.Mul_exprContext):
        pass

        # Enter a parse tree produced by matlabParser#pow_expr.

    def enterPow_expr(self, ctx: matlabParser.Pow_exprContext):
        self.my_list.append(" ** ")

        # Exit a parse tree produced by matlabParser#pow_expr.

    def exitPow_expr(self, ctx: matlabParser.Pow_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#colon.
    def enterColon(self, ctx: matlabParser.ColonContext):
        self.my_list.append(": ")

    # Exit a parse tree produced by matlabParser#colon.
    def exitColon(self, ctx: matlabParser.ColonContext):
        pass

    # Enter a parse tree produced by matlabParser#semicolon.
    def enterSemicolon(self, ctx: matlabParser.SemicolonContext):
        self.my_list.append("\n")

    # Exit a parse tree produced by matlabParser#semicolon.
    def exitSemicolon(self, ctx: matlabParser.SemicolonContext):
        pass

    # Enter a parse tree produced by matlabParser#and_expr.
    def enterAnd_expr(self, ctx: matlabParser.And_exprContext):
        self.my_list.append("and ")

    # Exit a parse tree produced by matlabParser#and_expr.
    def exitAnd_expr(self, ctx: matlabParser.And_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#or_expr.
    def enterOr_expr(self, ctx: matlabParser.Or_exprContext):
        self.my_list.append("or ")

    # Exit a parse tree produced by matlabParser#or_expr.
    def exitOr_expr(self, ctx: matlabParser.Or_exprContext):
        pass

    # Enter a parse tree produced by matlabParser#point.
    def enterPoint(self, ctx: matlabParser.PointContext):
        self.my_list.append(", ")

    # Exit a parse tree produced by matlabParser#point.
    def exitPoint(self, ctx: matlabParser.PointContext):
        pass
