# Generated from C:/Users/Janek/Kompilatory/Matlab2Python\matlab.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2*")
        buf.write("\u00d9\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\3\2\3\2\3\3\3\3\3\4\3\4\3\5")
        buf.write("\3\5\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\n\3\n\3")
        buf.write("\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20")
        buf.write("\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31")
        buf.write("\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34")
        buf.write("\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3")
        buf.write("!\3!\3\"\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3")
        buf.write("(\3)\6)\u00d4\n)\r)\16)\u00d5\3)\3)\2\2*\3\3\5\4\7\5\t")
        buf.write("\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20")
        buf.write("\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65")
        buf.write("\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*\3\2\5\3\2\62")
        buf.write(";\4\2C\\c|\5\2\13\f\17\17\"\"\2\u00d9\2\3\3\2\2\2\2\5")
        buf.write("\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2")
        buf.write("\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2")
        buf.write("\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2")
        buf.write("\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2")
        buf.write("\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61")
        buf.write("\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2")
        buf.write("\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3")
        buf.write("\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M")
        buf.write("\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\3S\3\2\2\2\5U\3\2\2\2\7")
        buf.write("W\3\2\2\2\tY\3\2\2\2\13[\3\2\2\2\r]\3\2\2\2\17`\3\2\2")
        buf.write("\2\21c\3\2\2\2\23e\3\2\2\2\25g\3\2\2\2\27i\3\2\2\2\31")
        buf.write("k\3\2\2\2\33m\3\2\2\2\35o\3\2\2\2\37q\3\2\2\2!t\3\2\2")
        buf.write("\2#w\3\2\2\2%z\3\2\2\2\'\u0083\3\2\2\2)\u0087\3\2\2\2")
        buf.write("+\u008d\3\2\2\2-\u0090\3\2\2\2/\u0095\3\2\2\2\61\u009c")
        buf.write("\3\2\2\2\63\u00a0\3\2\2\2\65\u00a7\3\2\2\2\67\u00ad\3")
        buf.write("\2\2\29\u00b6\3\2\2\2;\u00b8\3\2\2\2=\u00ba\3\2\2\2?\u00bd")
        buf.write("\3\2\2\2A\u00c0\3\2\2\2C\u00c3\3\2\2\2E\u00c6\3\2\2\2")
        buf.write("G\u00c8\3\2\2\2I\u00ca\3\2\2\2K\u00cc\3\2\2\2M\u00ce\3")
        buf.write("\2\2\2O\u00d0\3\2\2\2Q\u00d3\3\2\2\2ST\7\60\2\2T\4\3\2")
        buf.write("\2\2UV\7a\2\2V\6\3\2\2\2WX\7<\2\2X\b\3\2\2\2YZ\7=\2\2")
        buf.write("Z\n\3\2\2\2[\\\7?\2\2\\\f\3\2\2\2]^\7(\2\2^_\7(\2\2_\16")
        buf.write("\3\2\2\2`a\7~\2\2ab\7~\2\2b\20\3\2\2\2cd\7.\2\2d\22\3")
        buf.write("\2\2\2ef\7-\2\2f\24\3\2\2\2gh\7/\2\2h\26\3\2\2\2ij\7,")
        buf.write("\2\2j\30\3\2\2\2kl\7\61\2\2l\32\3\2\2\2mn\7`\2\2n\34\3")
        buf.write("\2\2\2op\7\u0080\2\2p\36\3\2\2\2qr\7\60\2\2rs\7,\2\2s")
        buf.write(" \3\2\2\2tu\7\60\2\2uv\7\61\2\2v\"\3\2\2\2wx\7\60\2\2")
        buf.write("xy\7`\2\2y$\3\2\2\2z{\7h\2\2{|\7w\2\2|}\7p\2\2}~\7e\2")
        buf.write("\2~\177\7v\2\2\177\u0080\7k\2\2\u0080\u0081\7q\2\2\u0081")
        buf.write("\u0082\7p\2\2\u0082&\3\2\2\2\u0083\u0084\7h\2\2\u0084")
        buf.write("\u0085\7q\2\2\u0085\u0086\7t\2\2\u0086(\3\2\2\2\u0087")
        buf.write("\u0088\7y\2\2\u0088\u0089\7j\2\2\u0089\u008a\7k\2\2\u008a")
        buf.write("\u008b\7n\2\2\u008b\u008c\7g\2\2\u008c*\3\2\2\2\u008d")
        buf.write("\u008e\7k\2\2\u008e\u008f\7h\2\2\u008f,\3\2\2\2\u0090")
        buf.write("\u0091\7g\2\2\u0091\u0092\7n\2\2\u0092\u0093\7u\2\2\u0093")
        buf.write("\u0094\7g\2\2\u0094.\3\2\2\2\u0095\u0096\7g\2\2\u0096")
        buf.write("\u0097\7n\2\2\u0097\u0098\7u\2\2\u0098\u0099\7g\2\2\u0099")
        buf.write("\u009a\7k\2\2\u009a\u009b\7h\2\2\u009b\60\3\2\2\2\u009c")
        buf.write("\u009d\7g\2\2\u009d\u009e\7p\2\2\u009e\u009f\7f\2\2\u009f")
        buf.write("\62\3\2\2\2\u00a0\u00a1\7t\2\2\u00a1\u00a2\7g\2\2\u00a2")
        buf.write("\u00a3\7v\2\2\u00a3\u00a4\7w\2\2\u00a4\u00a5\7t\2\2\u00a5")
        buf.write("\u00a6\7p\2\2\u00a6\64\3\2\2\2\u00a7\u00a8\7d\2\2\u00a8")
        buf.write("\u00a9\7t\2\2\u00a9\u00aa\7g\2\2\u00aa\u00ab\7c\2\2\u00ab")
        buf.write("\u00ac\7m\2\2\u00ac\66\3\2\2\2\u00ad\u00ae\7e\2\2\u00ae")
        buf.write("\u00af\7q\2\2\u00af\u00b0\7p\2\2\u00b0\u00b1\7v\2\2\u00b1")
        buf.write("\u00b2\7k\2\2\u00b2\u00b3\7p\2\2\u00b3\u00b4\7w\2\2\u00b4")
        buf.write("\u00b5\7g\2\2\u00b58\3\2\2\2\u00b6\u00b7\7>\2\2\u00b7")
        buf.write(":\3\2\2\2\u00b8\u00b9\7@\2\2\u00b9<\3\2\2\2\u00ba\u00bb")
        buf.write("\7>\2\2\u00bb\u00bc\7?\2\2\u00bc>\3\2\2\2\u00bd\u00be")
        buf.write("\7@\2\2\u00be\u00bf\7?\2\2\u00bf@\3\2\2\2\u00c0\u00c1")
        buf.write("\7?\2\2\u00c1\u00c2\7?\2\2\u00c2B\3\2\2\2\u00c3\u00c4")
        buf.write("\7\u0080\2\2\u00c4\u00c5\7?\2\2\u00c5D\3\2\2\2\u00c6\u00c7")
        buf.write("\7*\2\2\u00c7F\3\2\2\2\u00c8\u00c9\7+\2\2\u00c9H\3\2\2")
        buf.write("\2\u00ca\u00cb\7]\2\2\u00cbJ\3\2\2\2\u00cc\u00cd\7_\2")
        buf.write("\2\u00cdL\3\2\2\2\u00ce\u00cf\t\2\2\2\u00cfN\3\2\2\2\u00d0")
        buf.write("\u00d1\t\3\2\2\u00d1P\3\2\2\2\u00d2\u00d4\t\4\2\2\u00d3")
        buf.write("\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d3\3\2\2\2")
        buf.write("\u00d5\u00d6\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00d8\b")
        buf.write(")\2\2\u00d8R\3\2\2\2\4\2\u00d5\3\b\2\2")
        return buf.getvalue()


class matlabLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    ADD = 9
    SUB = 10
    MUL = 11
    DIV = 12
    POW = 13
    NOT = 14
    ARRAY_MUL = 15
    ARRAY_DIV = 16
    ARRAY_POW = 17
    FUNCTION = 18
    FOR = 19
    WHILE = 20
    IF = 21
    ELSE = 22
    ELSEIF = 23
    END = 24
    RETURN = 25
    BREAK = 26
    CONTINUE = 27
    LOWER = 28
    GREATER = 29
    LOWER_EQUAL = 30
    GREATER_EQUAL = 31
    EQUAL = 32
    NOT_EQUAL = 33
    OPEN_BRACKET = 34
    CLOSE_BRACKET = 35
    OPEN_SQ_BRACKET = 36
    CLOSE_SQ_BRACKET = 37
    NUMBER = 38
    LETTER = 39
    WS = 40

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'.'", "'_'", "':'", "';'", "'='", "'&&'", "'||'", "','", "'+'", 
            "'-'", "'*'", "'/'", "'^'", "'~'", "'.*'", "'./'", "'.^'", "'function'", 
            "'for'", "'while'", "'if'", "'else'", "'elseif'", "'end'", "'return'", 
            "'break'", "'continue'", "'<'", "'>'", "'<='", "'>='", "'=='", 
            "'~='", "'('", "')'", "'['", "']'" ]

    symbolicNames = [ "<INVALID>",
            "ADD", "SUB", "MUL", "DIV", "POW", "NOT", "ARRAY_MUL", "ARRAY_DIV", 
            "ARRAY_POW", "FUNCTION", "FOR", "WHILE", "IF", "ELSE", "ELSEIF", 
            "END", "RETURN", "BREAK", "CONTINUE", "LOWER", "GREATER", "LOWER_EQUAL", 
            "GREATER_EQUAL", "EQUAL", "NOT_EQUAL", "OPEN_BRACKET", "CLOSE_BRACKET", 
            "OPEN_SQ_BRACKET", "CLOSE_SQ_BRACKET", "NUMBER", "LETTER", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "ADD", "SUB", "MUL", "DIV", "POW", "NOT", "ARRAY_MUL", 
                  "ARRAY_DIV", "ARRAY_POW", "FUNCTION", "FOR", "WHILE", 
                  "IF", "ELSE", "ELSEIF", "END", "RETURN", "BREAK", "CONTINUE", 
                  "LOWER", "GREATER", "LOWER_EQUAL", "GREATER_EQUAL", "EQUAL", 
                  "NOT_EQUAL", "OPEN_BRACKET", "CLOSE_BRACKET", "OPEN_SQ_BRACKET", 
                  "CLOSE_SQ_BRACKET", "NUMBER", "LETTER", "WS" ]

    grammarFileName = "matlab.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


