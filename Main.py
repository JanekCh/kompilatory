from antlr4 import *
from gen.matlabLexer import matlabLexer
from gen.matlabParser import matlabParser
from gen.matlabListenerImplementation import matlabListenerImplementation


def main(argv):
    input_stream = FileStream(argv)
    lexer = matlabLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = matlabParser(stream)
    tree = parser.program()
    printer = matlabListenerImplementation()
    walker = ParseTreeWalker()
    walker.walk(printer, tree)


if __name__ == '__main__':
    main(r"test")